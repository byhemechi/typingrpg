set colorcolumn=80

function Runcode()
	split | terminal ./run.sh
endfunction

map <F5> :call Runcode()<CR>

set nowrap
set splitbelow
