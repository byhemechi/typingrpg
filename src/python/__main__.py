################################################################################
##                                                                            ##
##  Title Screen                                                              ##
##  Quick note: This is a complete rewrite. The original version was a decent ##
##  bit faster; but had bits of js, dart and c++ in it and it really was      ##
##  cheating, and I couldn't submit it in good conscience.                    ##
##                                                                            ##
################################################################################
import shutil
from Getch import _Getch
getch = _Getch()
width, height = shutil.get_terminal_size((80, 20))                             # Get the current terminal size, used for centring text

title = "\n" * 5 + """                                                        
 _____     _        _ _         _               _ _     ____  ____   ____  
|_   _|__ | |_ __ _| | |_   _  | |    ___  __ _(_) |_  |  _ \|  _ \ / ___|
  | |/ _ \| __/ _` | | | | | | | |   / _ \/ _` | | __| | |_) | |_) | |  _ 
  | | (_) | || (_| | | | |_| | | |__|  __/ (_| | | |_  |  _ <|  __/| |_| |
  |_|\___/ \__\__,_|_|_|\__, | |_____\___|\__, |_|\__| |_| \_\_|    \____|
                        |___/             |___/                           

Press the any key\
"""                                                                            # Block string, note the trailing spaces: these are *really* needed for alignment

if width < 73:
    title = "\033[1m\nTotally Legit RPG\n\033[0m\nPress the any key"

titlesplit = title.split("\n")                                                 # Split the blockstring by linebreaks into a list so we can iterate over them

for helptext in titlesplit:
    indent = width / 2 - len(helptext) / 2                                     # Calculate the number of columns the line should be offset by
    print(" " * int(indent) + helptext)

getch()                                                                        # Wait for a keypress
